enum Commands: Int {
    case scalarQuery
    case withJOIN
    case usingOTV
    case metadata
    case callScalarFrom3Lab
    case callTableFrom3Lab
    case callProcFrom3Lab
    case systemFunc
    case createTable
    case fillTable
    case quit
}
