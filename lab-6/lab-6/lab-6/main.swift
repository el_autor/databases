import Foundation
import PostgresClientKit

func showMenu() {
    print("""
    1) Cкалярный запрос
    2) Запрос с несколькими соединениями (JOIN)
    3) Запрос с ОТВ(CTE) и оконными функциями
    4) Запрос к метаданным
    5) Вызов скалярной функции (написанной в 3 л/р)
    6) Вызов табличной функции (написанной в 3 л/р)
    7) Вызов хранимой процедуры (написанной в 3 л/р)
    8) Вызов системной функции
    9) Создать таблицу в базе данных
    10) Выполнить вставку данных в созданную таблицу
    11) Выйти
    """)
}

do {
    var configuration = PostgresClientKit.ConnectionConfiguration()
    
    configuration.host = "localhost"
    configuration.port = 5432
    configuration.database = "ufc"
    configuration.user = "postgres"
    configuration.ssl = false
    configuration.credential = .md5Password(password: "zVQ2zYTe")
    
    let connection = try PostgresClientKit.Connection(configuration: configuration)

    var loop = true

    showMenu()

    while loop {
        let commandNumber = Int(readLine()!)
        let command = Commands(rawValue: commandNumber! - 1)
        
        switch command {
        case .scalarQuery:
            let text = """
            SELECT COUNT(*)
            FROM fighters;
            """
            
            let statement = try connection.prepareStatement(text: text)
            let response = try statement.execute()
            
            for row in response {
                let columns = try row.get().columns
                print("Количеcтво строк в таблице fighters: \(try columns[0].int())")
            }
        case .withJOIN:
            print("Введите имя бойца:")
            let fighterName = readLine()!
            
            let text = """
            SELECT e.location, e.name
            FROM events as e
            JOIN fights ON e.id = fights.id_event
            JOIN fighters as f ON fights.id_fighter_r = f.id
            WHERE f.name = '\(fighterName)';
            """
            
            let statement = try connection.prepareStatement(text: text)
            let response = try statement.execute()
            
            print("Эвенты, где бился \(fighterName)")
            
            for row in response {
                let columns = try row.get().columns
                let place = try columns[0].string()
                let eventName = try columns[0].string()
                
                print("Place - \(place), EventName - \(eventName)")
            }
        case .usingOTV:
            let text = """
            WITH temp (id, id_event, id_fighter_r, id_fighter_b, total_strikes)
            AS
            (
                SELECT id, id_event, id_fighter_r, id_fighter_b, r_total_strikes + b_sig_strikes
                FROM fights
                WHERE r_total_strikes + b_sig_strikes >= 350
            )
            SELECT id_fighter_r, id_fighter_b, id_event,
                COUNT(*)
                OVER (PARTITION BY id_event) as fights_per_event
            FROM temp;
            """
            
            let statement = try connection.prepareStatement(text: text)
            let response = try statement.execute()
            
            for row in response {
                let columns = try row.get().columns
                
                let red_fighter = try columns[0].int()
                let blue_fighter = try columns[1].int()
                let eventID = try columns[2].int()
                let fightsPerEvent = try columns[3].int()
                
                print("Red - \(red_fighter), Blue - \(blue_fighter), EventID - \(eventID), FightsPerEvent - \(fightsPerEvent)")
            }
        case .metadata:
            let text = """
            SELECT table_name, pg_relation_size(cast(table_name as varchar)) as size
            FROM information_schema.tables
            WHERE table_schema = 'public'
            ORDER BY size DESC;
            """
            
            let statement = try connection.prepareStatement(text: text)
            let response = try statement.execute()
            
            for row in response {
                let columns = try row.get().columns
                
                let tableName = try columns[0].string()
                let size = try columns[1].int()
                
                print("TableName - \(tableName), Size - \(size)")
            }
        case .callScalarFrom3Lab:
            print("Введите id 1 бойца")
            let idFirst = Int(readLine()!)!
            
            print("Введите id 2 бойца")
            let idSecond = Int(readLine()!)!
            
            let text = """
            SELECT sumUp(\(idFirst), \(idSecond));
            """
            
            let statement = try connection.prepareStatement(text: text)
            let response = try statement.execute()
            
            for row in response {
                let columns = try row.get().columns
                
                let totalFights = try columns[0].int()
                
                print("TotalFights - \(totalFights)")
            }
            
        case .callTableFrom3Lab:
            let text = """
            SELECT getFightsForFighter(22);
            """
            
            let statement = try connection.prepareStatement(text: text)
            let response = try statement.execute()
            
            for row in response {
                let columns = try row.get().columns
                let desc = columns[0].description
                
                print("(RedID, BlueID) - \(desc)")
            }
        case .callProcFrom3Lab:
            let text = """
            CALL insert_referee('Chang Chong', 2131);
            """
            
            let statement = try connection.prepareStatement(text: text)
            let _ = try statement.execute()
        case .systemFunc:
            let text = """
            SELECT inet_server_port();
            """
            
            let statement = try connection.prepareStatement(text: text)
            let response = try statement.execute()
            
            for row in response {
                let columns = try row.get().columns
                let port = try columns[0].int()
                
                print("ServerPort - \(port)")
            }
            
        case .createTable:
            let text = """
            CREATE TABLE extra_stats (
                fighter_id int,
                average_strikes double precision
            );
            """
            
            let statement = try connection.prepareStatement(text: text)
            let _ = try statement.execute()
        case .fillTable:
            let text = """
            INSERT INTO extra_stats (fighter_id, average_strikes)
            SELECT f.id, avg(fights.r_total_strikes + fights.b_total_strikes)
            FROM fighters AS f
            JOIN fights ON f.id = fights.id_fighter_b OR f.id = fights.id_fighter_r
            GROUP BY f.id;
            """
            
            let statement = try connection.prepareStatement(text: text)
            let _ = try statement.execute()
        case .quit:
            loop = false
        default:
            ()
        }
    }
    
} catch {
    print(error)
}
