-- 1) Извлекаем фрагмент

SELECT doc -> 'records' AS records
FROM fighters_extra_json;

-- 2) Извлекаем значения конкретных узлов

SELECT doc -> 'records' ->> 'total_bonuses' AS bonuses
FROM fighters_extra_json;  

-- 3) Проверка существования узла или атрибута

SELECT doc @> '{"id": 5}'::jsonb bool_flag
FROM fighters_extra_json;

SELECT doc ? 'alias' bool_flag
FROM fighters_extra_json;

-- 4) Изменить JSON

UPDATE fighters_extra_json
SET doc = jsonb_set(doc, '{name}', '"Nick Diaz"')
WHERE doc ->> 'name' = 'Nate Diaz';

\COPY (SELECT doc FROM fighters_extra_json) TO '/Users/vlad/Downloads/databases/lab-5/json_data/extra.json';

-- 5) Разделить JSON на несколько строк по узлам

\COPY (SELECT jsonb_pretty(doc) FROM fighters_extra_json) TO '/Users/vlad/Downloads/databases/lab-5/json_data/pretty.json';

