DROP TABLE referees_copy;
DROP TABLE referees_json;

CREATE TABLE referees_copy (
    id int,
    name varchar NOT NULL,
    experience int NOT NULL
);

CREATE TABLE referees_json (
    doc jsonb NOT NULL
);

\COPY referees_json FROM '/Users/vlad/Downloads/databases/lab-5/json_data/referees.json';

INSERT INTO referees_copy (id, name, experience)
SELECT CAST (doc ->> 'id' AS int) AS id, doc ->> 'name' AS name, CAST (doc ->> 'experience' AS int) AS experience
FROM referees_json;