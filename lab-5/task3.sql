DROP TABLE fighters_extra_json;

CREATE TABLE fighters_extra_json (
    doc jsonb NOT NULL
);

INSERT INTO fighters_extra_json (doc)
VALUES ('{"id":1, "name":"Nate Diaz", "records":{"total_fights":10, "max_strikes":178, "total_bonuses":2}}'),
       ('{"id":2, "name":"Conor Mcgregor", "records":{"total_fights":17, "max_strikes":191, "total_bonuses":8}}'),
       ('{"id":3, "name":"Dustin Poirier", "records":{"total_fights":13, "max_strikes":54, "total_bonuses":3}}'),
       ('{"id":4, "name":"John Jones", "records":{"total_fights":14, "max_strikes":168, "total_bonuses":5}}'),
       ('{"id":5, "name":"Tony Ferguson", "records":{"total_fights":2, "max_strikes":121, "total_bonuses":4}}'),
       ('{"id":6, "name":"Michael Chandler", "records":{"total_fights":6, "max_strikes":135, "total_bonuses":3}}'),
       ('{"id":7, "name":"Dan Hooker", "records":{"total_fights":9, "max_strikes":118, "total_bonuses":0}}'),
       ('{"id":8, "name":"Edson Barboza", "records":{"total_fights":3, "max_strikes":87, "total_bonuses":6}}'),
       ('{"id":9, "name":"Francis Ngannou", "records":{"total_fights":12, "max_strikes":94, "total_bonuses":8}}'),
       ('{"id":10, "name":"Mirco Crocop", "records":{"total_fights":7, "max_strikes":37, "total_bonuses":1}}');

