select winner, name_r, name_b, id_fighter_r, id_fighter_b
from fights join
	 (
	 	select id, name as name_r
	 	from fighters
	 ) as names_r on id_fighter_r = names_r.id
	 	join
	 (
	 	select id, name as name_b
	 	from fighters
	 ) as names_b on id_fighter_b = names_b.id
	 