select location, name, fightday 
from events
where id in 
	  (
	  		select id_event
	  		from fights
	  		where id_fighter_b in
	  		      (
	  		      		select id
	  		      		from fighters
	  		      		where name in
	  		      			  (
	  		      			  		select name
	  		      			  		from referees
	  		      			  )
	  		      )	
	  		group by id_event
	  )
union
select location, name, fightday 
from events
where id in
	  (
	  		select id_event
	  		from fights
	  		where id_fighter_r in
	  		      (
	  		      		select id
	  		      		from fighters
	  		      		where name in
	  		      			  (
	  		      			  		select name
	  		      			  		from referees
	  		      			  )
	  		      )	
	  		group by id_event
	  )
order by fightday desc