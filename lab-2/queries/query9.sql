select name,
	case extract(year from date_of_birth)
		when 1970 then 'elderly'
		when 1980 then 'veteran'
		else 'current'
	end as type
from fighters;
