select distinct id_event,
       (
       		select avg(r_total_strikes + b_total_strikes)
       		from fights
       		where id_event = outter_fights.id_event 
       ),
       (
       		select max(r_total_strikes + b_total_strikes)
       		from fights
       		where id_event = outter_fights.id_event 
       )
from fights as outter_fights;
       