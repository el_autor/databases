select name,
	   (wins + loses + draws) as total_fights,
	   kos
into wins_attitude
from fighters
where (wins + loses + draws) != 0;

/*
select name, kos / cast(total_fights as numeric) as attitude
from kos_attitude;
*/