select id_fighter_b, id_fighter_r,
	count(*)
	over (partition by id_event) as fights_per_event,
	avg(r_total_strikes)
	over (partition by id_event) as total_strikes_red_corner,
	avg(b_total_strikes)
	over (partition by id_event) as total_strikes_blue_corner
from fights;