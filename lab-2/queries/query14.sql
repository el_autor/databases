select name_ref, count(id_referee) as total_fights
from fights join
		    (
		    	select id, name as name_ref
		    	from referees
			) as names_r on names_r.id = fights.id_referee 
where id_referee != 1
group by name_ref;
