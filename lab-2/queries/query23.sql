with recursive RecursiveQuery (id, name, parent_id)
as
(
    select id, name, parent_id
    from weightclasses
    where weightclasses.id = 3
    union all
    select weightclasses.id, weightclasses.name, weightclasses.parent_id
    from weightclasses
    join RecursiveQuery on weightclasses.parent_id = RecursiveQuery.id
)
select id, name, parent_id
from RecursiveQuery;
