select name
from fighters
where exists
	 (
	 	select id_fighter_r, id_fighter_b
	 	from fights
	 	where (id_fighter_r = fighters.id and r_sig_strikes >= 200) or (id_fighter_b = fighters.id and b_sig_strikes >= 200)
	 );