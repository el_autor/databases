with tmp (fight_id, total_strikes)
as
(
	select id, r_total_strikes + b_sig_strikes
	from fights
	where r_total_strikes + b_sig_strikes >= 300
)
select avg(total_strikes) as average
from tmp;