select name,
	case
		when wins <= loses then 'need to be fired'
		when wins >= loses + 8 then 'superstar'
		when wins > loses + 5 then 'star'
		when wins > loses then 'normal'
	end as fighter_desc
from fighters;