with tmp (id_event, fights_per_event, total_strikes_red_corner, total_strikes_blue_corner, numeration)
as
(
    select id_event,
        count(*)
        over (partition by id_event) as fights_per_event,
        avg(r_total_strikes)
        over (partition by id_event) as total_strikes_red_corner,
        avg(b_total_strikes)
        over (partition by id_event) as total_strikes_blue_corner,
        row_number() 
       	over (partition by id_event) as numeration
    from fights
)
select id_event, fights_per_event, total_strikes_red_corner, total_strikes_blue_corner
from tmp
where numeration = 1;