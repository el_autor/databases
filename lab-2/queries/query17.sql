insert into referees (name, experience)
select name, (wins + loses + draws) as total_fights
from fighters
where wins + loses + draws >= 30; 