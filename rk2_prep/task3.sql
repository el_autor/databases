CREATE OR REPLACE PROCEDURE get_indexes(db_name varchar, table_name varchar)
LANGUAGE plpgsql
AS
$$
DECLARE
    rec record;
    BEGIN
        FOR rec IN (SELECT indexname, indexdef FROM pg_indexes WHERE tablename = table_name)
        LOOP
            RAISE NOTICE 'indexname : % , indexdef : %', rec.indexname, rec.indexdef;
        END LOOP;
    END;
$$;

CALL get_indexes('rk2', 'employee');