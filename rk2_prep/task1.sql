DROP DATABASE rk2;
CREATE DATABASE rk2;
\c rk2

-- Создание таблиц

CREATE TABLE department (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    name varchar,
    tel varchar,
    manager_id int
);

CREATE TABLE employee (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    department_id int NOT NULL,
    fio varchar,
    position varchar,
    salary int,
    FOREIGN KEY (department_id) REFERENCES department(id)
);

CREATE TABLE medicine (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    name varchar,
    instruction varchar,
    cost int
);

CREATE TABLE EmpMed (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    medicine_id int,
    employee_id int,
    FOREIGN KEY (medicine_id) REFERENCES medicine(id),
    FOREIGN KEY (employee_id) REFERENCES employee(id)
);

INSERT INTO department (name, tel, manager_id)
VALUES ('first', '1000', 1);
INSERT INTO department (name, tel, manager_id)
VALUES ('second', '2000', 2);
INSERT INTO department (name, tel, manager_id)
VALUES ('third', '3000', 3);
INSERT INTO department (name, tel, manager_id)
VALUES ('fourth', '4000', 4);
INSERT INTO department (name, tel, manager_id)
VALUES ('fifth', '5000', 5);

INSERT INTO employee (department_id, fio, position, salary)
VALUES (1, 'Arsen', 'Junior', 50);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (1, 'Venger', 'Middle', 100);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (1, 'Kolya', 'Senior', 150);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (2, 'Uriy', 'Junior', 50);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (3, 'Efim', 'Junior', 50);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (4, 'Sarkis', 'Junior', 50);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (4, 'Bruh', 'Senior', 100);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (4, 'Young thug', 'Junior', 50);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (5, 'Old thug', 'Junior', 50);
INSERT INTO employee (department_id, fio, position, salary)
VALUES (5, 'Avilla', 'Junior', 50);

INSERT INTO medicine (name, instruction, cost)
VALUES ('Miromistin', 'For all', 100);
INSERT INTO medicine (name, instruction, cost)
VALUES ('Coronavir', 'For corona', 1000);
INSERT INTO medicine (name, instruction, cost)
VALUES ('Arbidol', 'For cough', 300);


INSERT INTO EmpMed (medicine_id, employee_id)
VALUES (1, 2);
INSERT INTO EmpMed (medicine_id, employee_id)
VALUES (1, 5);
INSERT INTO EmpMed (medicine_id, employee_id)
VALUES (2, 3);
INSERT INTO EmpMed (medicine_id, employee_id)
VALUES (2, 1);
INSERT INTO EmpMed (medicine_id, employee_id)
VALUES (3, 7);
INSERT INTO EmpMed (medicine_id, employee_id)
VALUES (3, 4);

ALTER TABLE department ADD FOREIGN KEY (manager_id) REFERENCES employee(id);