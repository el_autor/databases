SELECT department_id, count(department_id) as tot_employees_per_dep
FROM employee
GROUP BY department_id
HAVING AVG(salary) > 50;