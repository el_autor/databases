SELECT fio,
    COUNT(*)
    OVER (PARTITION BY department_id) AS employees_per_dep,
    AVG(SALARY)
    OVER (PARTITION BY department_id) AS avg_dep_salary
FROM employee;