CREATE OR REPLACE PROCEDURE get_table_size()
LANGUAGE SQL
AS $$
    SELECT table_name, pg_relation_size(cast(table_name as varchar)) as size
    INTO local_table
    FROM information_schema.tables
    WHERE table_schema = 'public'
    ORDER BY size DESC;
$$;

CALL get_table_size();

SELECT *
FROM local_table;