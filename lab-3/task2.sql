-- подставляемая табличная функция
-- достаем всех оппонентов для бойца с заданным id

DROP FUNCTION getFightsForFighter(int);

CREATE FUNCTION getFightsForFighter(int) RETURNS SETOF fights AS $$
    SELECT * from fights
    WHERE id_fighter_b = $1 OR id_fighter_r = $1;
$$ LANGUAGE SQL;

SELECT id_fighter_r, id_fighter_b
FROM getFightsForFighter(12);