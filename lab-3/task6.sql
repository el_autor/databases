CREATE OR REPLACE PROCEDURE delete_child_weightclass(int)
LANGUAGE SQL
AS $$
    WITH RECURSIVE RecursiveQuery (id, name, parent_id)
    AS
    (
        SELECT id, name, parent_id
        FROM weightclasses
        WHERE weightclasses.id = $1

        UNION ALL

        SELECT weightclasses.id, weightclasses.name, weightclasses.parent_id
        FROM weightclasses
        JOIN RecursiveQuery ON weightclasses.parent_id = RecursiveQuery.id
    )
    DELETE FROM weightclasses
    WHERE id != $1 AND id IN (SELECT id FROM RecursiveQuery);
$$;

CALL delete_child_weightclass(3);