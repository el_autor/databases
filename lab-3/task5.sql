-- добавляет нового рефери

CREATE OR REPLACE PROCEDURE insert_referee(name varchar, experience int)
LANGUAGE SQL
AS $$
insert into referees (name, experience)
values (name, experience);
$$;

call insert_referee('Nilas Mochombo', 109);