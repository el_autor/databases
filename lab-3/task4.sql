--  рекурсивная функция или функция с рекурсивным ОТВ
-- выводит дочернюю весовую категории для заданной родительской

DROP FUNCTION get_child_category(int);

CREATE FUNCTION get_child_category(int) RETURNS TABLE(id int, name varchar, parent_id int) AS $$
    WITH RECURSIVE RecursiveQuery (id, name, parent_id)
    AS
    (
        SELECT id, name, parent_id
        FROM weightclasses
        WHERE weightclasses.id = $1

        UNION ALL

        SELECT weightclasses.id, weightclasses.name, weightclasses.parent_id
        FROM weightclasses
        JOIN RecursiveQuery ON weightclasses.parent_id = RecursiveQuery.id
    )
    SELECT id, name, parent_id
    FROM RecursiveQuery;
$$ LANGUAGE SQL;

SELECT *
FROM get_child_category(3);
