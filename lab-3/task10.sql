DROP VIEW referees_view;
DROP TRIGGER instead_of_insertion;

CREATE VIEW referees_view AS
SELECT *
FROM referees;

CREATE OR REPLACE FUNCTION execution_instead_trigger() RETURNS TRIGGER 
AS $$
    BEGIN

    RAISE NOTICE ' Удаление запрещено';
    RETURN old;

    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER instead_of_insertion
INSTEAD OF DELETE ON referees_view
FOR EACH ROW
EXECUTE PROCEDURE execution_instead_trigger();

DELETE FROM referees_view
WHERE name = 'Herb Dean';