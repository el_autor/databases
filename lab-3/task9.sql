-- 5) Триггер

DROP TRIGGER insert_notifier on referees;

CREATE OR REPLACE FUNCTION notify() RETURNS TRIGGER
AS $$
    BEGIN
        RAISE NOTICE 'Произведена запись в таблицу referee: name(%), exp(%)', new.name, new.experience;
        RETURN new;
    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER insert_notifier
AFTER INSERT ON referees
FOR EACH ROW
EXECUTE PROCEDURE notify();

INSERT INTO referees (name, experience)
VALUES ('Pogba', 123); 