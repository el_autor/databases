-- скалярная функция

CREATE OR REPLACE FUNCTION sumUp(id_r integer, id_b integer) RETURNS bigint AS $$
    SELECT COUNT(*)
    FROM fights
    WHERE id_fighter_r = id_r OR id_fighter_b = id_b;
$$ LANGUAGE SQL;

SELECT *
from sumUP(10, 20);