-- процедура с курсором
-- удаляем бои где участвовал боец с заданным id

CREATE OR REPLACE PROCEDURE cursor_procedure(int)
LANGUAGE 'plpgsql'
AS $BODY$
    DECLARE
        my_cursor CURSOR FOR SELECT id_fighter_r, id_fighter_b FROM fights;

        _id_r int;
        _id_b int;

    BEGIN
        OPEN my_cursor;

        LOOP
        FETCH my_cursor INTO _id_r, _id_b;
        IF NOT FOUND THEN EXIT;END IF;

        IF _id_r = $1 OR _id_b = $1 THEN
            DELETE FROM fights WHERE CURRENT OF my_cursor;
        END IF;

        END LOOP;

        CLOSE my_cursor;
    END;
$BODY$;

call cursor_procedure(1226);