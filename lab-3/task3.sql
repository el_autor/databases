-- многооператорная табличная функция
-- достаем всех оппонентов для бойца с заданным id
DROP FUNCTION getfightsforfighter(integer);

CREATE OR REPLACE FUNCTION getFightsForFighter(int) RETURNS TABLE(red_corner int, blue_corner int) AS $$
    SELECT id_fighter_r, id_fighter_b from fights
    WHERE id_fighter_r = $1
    UNION
    SELECT id_fighter_r, id_fighter_b from fights
    WHERE id_fighter_b = $1;
$$ LANGUAGE SQL;

SELECT *
FROM getFightsForFighter(22);