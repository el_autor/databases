import SwiftyJSON
import PerfectCRUD
import PerfectPostgreSQL
import Foundation

struct Referees: Codable {
    let id: Int
    let name: String
    let experience: Int
    
    let fights: [Fights]?
}

struct Fights: Codable {
    let id: Int
    let idEvent: Int
    let idWeightclass: Int
    let idReferee: Int
    let idFighterR: Int
    let idFighterB: Int
    let rSigStrikes: Int
    let bSigStrikes: Int
    let rTotalStrikes: Int
    let bTotalStrikes: Int
    let winner: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case idEvent = "id_event"
        case idWeightclass = "id_weightclass"
        case idReferee = "id_referee"
        case idFighterR = "id_fighter_r"
        case idFighterB = "id_fighter_b"
        case rSigStrikes = "r_sig_strikes"
        case bSigStrikes = "b_sig_strikes"
        case rTotalStrikes = "r_total_strikes"
        case bTotalStrikes = "b_total_strikes"
        case winner = "winner"
    }
}

// MARK: TASK 1
let referees = [Referees(id: 0, name: "Arsen", experience: 50, fights: nil),
                Referees(id: 1, name: "Alex", experience: 100, fights: nil),
                Referees(id: 2, name: "Mike", experience: 150, fights: nil),
                Referees(id: 3, name: "Ilon", experience: 200, fights: nil),
                Referees(id: 4, name: "Tesla", experience: 250, fights: nil),
                Referees(id: 5, name: "Tesla", experience: 100, fights: nil)]

let acceptedNames = ["Alex", "Fred"]

// 1
let query1 = referees.map { ($0.name, $0.experience) }
print(query1)

// 2
let query2 = referees.filter { $0.id <= 2 }
print(query2)

// 3
let query3 = referees.reduce(.zero) { $0 + $1.experience }
print(query3)

let queryExtra = Dictionary(grouping: referees, by: { $0.experience }).map { ($0.key, $0.value.count) }
print(queryExtra)

// 4
let query4 = referees.filter({$0.id > 3}).count
print(query4)

// 5
let query5 = referees.filter {acceptedNames.contains($0.name)}
print(query5)
 
// MARK: TASK 2
/*
let db = Database(configuration: try PostgresDatabaseConfiguration(database: "ufc", host: "localhost", port: 5432, username: "postgres", password: "zVQ2zYTe"))

let refereesTable = db.table(Referees.self)
let fightsTable = db.table(Fights.self)

let query = try refereesTable.select()
let jsonData = query.map { try! JSONEncoder().encode($0) }

var jsonObjects = jsonData.map { try! JSON(data: $0) }

// Чтение
let choosed = jsonObjects.filter { $0["id"] <= 100 }
print(choosed)

// Обновление
for i in .zero..<jsonObjects.count {
    jsonObjects[i]["experience"].int! += 2
}

for i in .zero..<jsonObjects.count {
    print(jsonObjects[i]["experience"])
}

// Запись
let newRef = Referees(id: 500, name: "Pavarotti", experience: 500)
let jsonEncoded = try! JSONEncoder().encode(newRef)
let jsonDecoded = try JSON(data: jsonEncoded)

jsonObjects.append(jsonDecoded)

print(jsonObjects)
*/
// MARK: TASK 3
let db = Database(configuration: try PostgresDatabaseConfiguration(database: "ufc", host: "localhost", port: 5432, username: "postgres", password: "zVQ2zYTe"))

let refereesTable = db.table(Referees.self)
let fightsTable = db.table(Fights.self)
 
// 1 - однотабличный запрос на выборку
/*
let query = try refereesTable
    .where(\Referees.experience > 100)
    .select()

query.forEach { ref in
    print("id: \(ref.id), name: \(ref.name), experience: \(ref.experience)")
}
*/
// 2 - многотабличный запрос на выборку.

// исправить
/*
let query = try db.sql("SELECT id_referee AS id, name, experience FROM fights JOIN referees ON referees.id = fights.id_referee WHERE fights.id BETWEEN 100 AND 200",
                       Referees.self)
*/

let join = try db.table(Referees.self)
    .join(\.fights, on: \.id, equals: \.idReferee)
    .where(\Fights.id <= 10)

let refs = try join.select()

refs.forEach { ref in
    print(ref)
}

// 3 - 3 запроса на добавление, изменение и удаление данных в базе данных

/*
let newReferee = Referees(id: 0, name: "Jhan", experience: 256)
try refereesTable.insert([newReferee], setKeys: \Referees.name, \Referees.experience)


let newReferee = Referees(id: 395, name: "Jorneyman", experience: 200)
try db.table(Referees.self)
    .where(\Referees.id == newReferee.id)
    .update(newReferee, setKeys: \Referees.name, \Referees.experience)

let referee = Referees(id: 395, name: "Jorneyman", experience: 200)
let query = refereesTable.where(\Referees.id == referee.id)
try query.delete()
*/
// 4 - получение доступа к данным, выполняя только хранимую процедуру

/*
let insertProcedureText = """
CREATE OR REPLACE PROCEDURE insert_referee(name varchar, experience int)
LANGUAGE SQL
AS
$$
insert into referees (name, experience)
values (name, experience);
$$;
"""

let deleteProcedureText = """
CREATE OR REPLACE PROCEDURE delete_referee(int)
LANGUAGE SQL
AS
$$
DELETE FROM fights
WHERE $1 = fights.id_referee;
DELETE FROM referees
WHERE $1 = referees.id;
$$;
"""

try db.sql(insertProcedureText)
try db.sql(deleteProcedureText)

try db.sql("CALL insert_referee('who', 100)")
try db.sql("CALL delete_referee(397)")
*/
