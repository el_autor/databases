import XCTest

import lab_7Tests

var tests = [XCTestCaseEntry]()
tests += lab_7Tests.allTests()
XCTMain(tests)
