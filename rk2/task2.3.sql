-- С помощью запроса получаем фамилии и опыт тех заказчиков,
-- у которых опыт больше чем средний опыт по заказчикам,
-- которые родились в один год

SELECT fio, experience
FROM orderer AS ordererOut
WHERE experience > (
    SELECT AVG(experience)
    FROM orderer ordererIn
    WHERE ordererIn.year_of_birth = ordererOut.year_of_birth
);