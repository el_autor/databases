DROP DATABASE rk2;
CREATE DATABASE rk2;
\c rk2

CREATE TABLE orderer (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    fio varchar,
    year_of_birth int,
    experience int,
    tel varchar
);

CREATE TABLE performer (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    fio varchar,
    year_of_birth int,
    experience int,
    tel varchar
);

CREATE TABLE jobtype (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    hours_to_do int,
    equipment varchar
);

CREATE TABLE jo (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    jobtype_id int,
    orderer_id int,
    FOREIGN KEY (jobtype_id) REFERENCES jobtype(id),
    FOREIGN KEY (orderer_id) REFERENCES orderer(id)
);

CREATE TABLE jp (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    jobtype_id int,
    performer_id int,
    FOREIGN KEY (jobtype_id) REFERENCES jobtype(id),
    FOREIGN KEY (performer_id) REFERENCES performer(id)
);

CREATE TABLE po (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    orderer_id int,
    performer_id int,
    FOREIGN KEY (orderer_id) REFERENCES orderer(id),
    FOREIGN KEY (performer_id) REFERENCES performer(id)
);

INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Coco', 2000, 22, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Fook', 2001, 22, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Moso', 2002, 21, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Fork', 2003, 20, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Nuts', 2004, 19, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Diz', 2005, 18, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Who', 2006, 17, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Man', 2000, 16, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Woman', 2000, 15, 1000);
INSERT INTO orderer (fio, year_of_birth, experience, tel)
VALUES ('Samba', 2000, 14, 1000);


INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Coco', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Fook', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Moso', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Fork', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Nuts', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Diz', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Who', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Man', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Woman', 2000, 22, 1000);
INSERT INTO performer (fio, year_of_birth, experience, tel)
VALUES ('Samba', 2000, 22, 1000);

INSERT INTO jobtype (hours_to_do, equipment)
VALUES (5, 'Molot');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (10, 'Tapki');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (15, 'Push up');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (20, 'Paket');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (12, 'Topor');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (18, 'Money');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (3, 'ExtraMoney');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (1, 'Choppa');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (8, 'Lopata');
INSERT INTO jobtype (hours_to_do, equipment)
VALUES (29, 'Otboika');

INSERT INTO jo (jobtype_id, orderer_id)
VALUES (2, 7);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (1, 9);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (7, 1);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (5, 6);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (3, 3);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (9, 1);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (3, 7);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (8, 7);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (4, 2);
INSERT INTO jo (jobtype_id, orderer_id)
VALUES (9, 1);

INSERT INTO jp (jobtype_id, performer_id)
VALUES (1, 3);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (2, 4);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (7, 1);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (1, 7);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (9, 2);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (1, 2);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (1, 5);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (1, 6);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (1, 7);
INSERT INTO jp (jobtype_id, performer_id)
VALUES (1, 8);

INSERT INTO po (orderer_id, performer_id)
VALUES (9,2);
INSERT INTO po (orderer_id, performer_id)
VALUES (8,5);
INSERT INTO po (orderer_id, performer_id)
VALUES (7,1);
INSERT INTO po (orderer_id, performer_id)
VALUES (6,4);
INSERT INTO po (orderer_id, performer_id)
VALUES (5,5);
INSERT INTO po (orderer_id, performer_id)
VALUES (4,2);
INSERT INTO po (orderer_id, performer_id)
VALUES (3,3);
INSERT INTO po (orderer_id, performer_id)
VALUES (2,3);
INSERT INTO po (orderer_id, performer_id)
VALUES (3,2);
INSERT INTO po (orderer_id, performer_id)
VALUES (4,3);
