-- получает разбиение заказчиков по опыту, т.е сколько есть заказчиков с одним и тем же опытом
-- также выводит средний год рождения

SELECT DISTINCT experience,
    COUNT(*)
    OVER (PARTITION BY experience) AS total_orderers_with_experience,
    AVG(year_of_birth)
    OVER (PARTITION BY experience) AS avg_year_of_birth
FROM orderer;


