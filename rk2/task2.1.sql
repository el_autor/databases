-- получаем заказчиком, которые родились с 2000 по 2004 год и имеют опыт меньше или равный 20

SELECT fio
FROM orderer
WHERE year_of_birth BETWEEN 2000 AND 2004 AND experience <= 20;