-- 3) табличная функция
-- достаем всех оппонентов для бойца с заданным id

DROP FUNCTION getFightsForFighter(int);

CREATE OR REPLACE FUNCTION getFightsForFighter(fighter_id int) 
RETURNS TABLE(fight_id int, red_corner int, blue_corner int)
LANGUAGE plpython3u
AS
$$
    allFights = plpy.execute(f"""SELECT * FROM fights;""") 

    resultTable = []

    for fight in allFights:
        if fight['id_fighter_r'] == fighter_id or fight['id_fighter_b'] == fighter_id:
            resultTable.append(
                {
                    "fight_id" : fight["id"],
                    "red_corner" : fight["id_fighter_r"],
                    "blue_corner" : fight["id_fighter_b"]
                }
            )

    return resultTable
$$;

SELECT *
FROM getFightsForFighter(102);