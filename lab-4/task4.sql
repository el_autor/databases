-- 4) хранимая процедура
-- добавляем рефери

CREATE OR REPLACE PROCEDURE insert_referee(name varchar, experience int)
LANGUAGE plpython3u
AS
$$
    plpy.execute(f"""
        INSERT INTO referees (name, experience)
        VALUES ('{name}', {experience});
    """)
$$;

call insert_referee('Nilas Mochombo', 109);