-- 6)  пользовательский тип данных

DROP TYPE fighter_stats CASCADE;
DROP TABLE extra_fighter_stats;

CREATE TYPE fighter_stats AS (
    name varchar,
    average_strikes double precision
);

CREATE TABLE extra_fighter_stats (
    fighter_id integer,
    stats fighter_stats
);

CREATE OR REPLACE PROCEDURE insert_extra_fighter_info(fighter_id int, name varchar, average_strikes double precision)
LANGUAGE plpython3u
AS
$$
    plpy.execute(f"""
        INSERT INTO extra_fighter_stats
        VALUES ({fighter_id}, ROW('{name}', {average_strikes}));
    """)
$$;

CALL insert_extra_fighter_info(1, 'Francis Ngannou', 35.13);

SELECT *
FROM extra_fighter_stats;
