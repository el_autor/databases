-- 2) агрегатная функция складывает значения в столбце (тут считает общее кол-во ударов)

CREATE OR REPLACE FUNCTION plus(state int, arg int) 
RETURNS int
LANGUAGE plpython3u
AS
$$
    result = plpy.execute(f"""SELECT {state} + {arg} AS result""")

    return result[0]["result"]
$$;

CREATE OR REPLACE AGGREGATE my_aggregate(int)
(
    sfunc = plus,
    stype = int,
    initcond = 0
);

SELECT my_aggregate(r_total_strikes + b_total_strikes)
FROM fights;