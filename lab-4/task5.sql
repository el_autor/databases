-- 5)  Триггер

DROP TRIGGER insert_notifier on referees;

CREATE OR REPLACE FUNCTION notify()
RETURNS TRIGGER
LANGUAGE plpython3u
AS 
$$
    plpy.notice(f"""
        Произведена запись в таблицу referees: name - {TD['new']['name']}, experience - {TD['new']['experience']}
    """)
$$;

CREATE TRIGGER insert_notifier
AFTER INSERT ON referees
FOR EACH ROW
EXECUTE PROCEDURE notify();

INSERT INTO referees (name, experience)
VALUES ('Pogba', 123); 
