-- 1) Скалярная функция 
-- ищем кол-во боев, где бился боец c id_fighter

--доделать питоном
DROP EXTENSION plpython3u CASCADE;
CREATE EXTENSION plpython3u;

CREATE OR REPLACE FUNCTION calculate_total(id_fighter integer) 
RETURNS int 
LANGUAGE plpython3u
AS 
$$
    allFights = plpy.execute(f"""SELECT * FROM fights;""")

    total = 0

    for fight in allFights:
        if id_fighter == fight['id_fighter_r'] or id_fighter == fight['id_fighter_b']:
            total += 1

    return total
$$;

SELECT calculate_total(113);