CREATE database ufc;
\c ufc

--Создание сущностей

CREATE TABLE events (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    fightday date NOT NULL,
    location varchar NOT NULL,
    name varchar NOT NULL
);

CREATE TABLE referees (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    name varchar NOT NULL,
    experience int NOT NULL
);

CREATE TABLE weightclasses (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    name varchar NOT NULL,
    down_bound int NOT NULL,
    up_bound int NOT NULL
);

CREATE TABLE fighters (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    name varchar NOT NULL,
    height numeric,
    weight int,
    reach int,
    date_of_birth date,
    wins int NOT NULL CHECK (wins >= 0),
    KOs int NOT NULL CHECK (KOs >= 0),
    subs int NOT NULL CHECK (subs >= 0),
    desision int NOT NULL CHECK (desision >= 0),
    loses int NOT NULL CHECK (loses >= 0),
    draws int NOT NULL CHECK (draws >= 0),
    stance varchar
);

CREATE TABLE fights (
    id int generated always as identity(START WITH 1 INCREMENT BY 1) PRIMARY KEY,
    id_event int NOT NULL,
    id_weightclass int NOT NULL,
    id_referee int NOT NULL,
    id_fighter_R int NOT NULL,
    id_fighter_B int NOT NULL,
    FOREIGN KEY (id_fighter_R) REFERENCES fighters(id),
    FOREIGN KEY (id_fighter_B) REFERENCES fighters(id),
    FOREIGN KEY (id_event) REFERENCES events(id),
    FOREIGN KEY (id_weightclass) REFERENCES weightclasses(id),
    FOREIGN KEY (id_referee) REFERENCES referees(id),
    R_sig_strikes int CHECK (R_sig_strikes >= 0),
    B_sig_strikes int CHECK (B_sig_strikes >= 0),
    R_total_strikes int CHECK (R_total_strikes >= 0),
    B_total_strikes int CHECK (B_total_strikes >= 0),
    winner varchar
);

\COPY weightclasses(name,down_bound,up_bound) FROM '/Users/vlad/Downloads/databases/lab-1/data/weightclasses.csv' DELIMITER ',' CSV HEADER;
\COPY referees(name,experience) FROM '/Users/vlad/Downloads/databases/lab-1/data/referees.csv' DELIMITER ',' CSV HEADER;
\COPY fighters(name,height,weight,reach,date_of_birth,wins,KOs,subs,desision,loses,draws,stance) FROM '/Users/vlad/Downloads/databases/lab-1/data/fighters.csv' DELIMITER ',' CSV HEADER;
\COPY events(fightday,location,name) FROM '/Users/vlad/Downloads/databases/lab-1/data/events.csv' DELIMITER ',' CSV HEADER;
\COPY fights(id_event,id_weightclass,id_referee,id_fighter_R,id_fighter_B,R_sig_strikes,B_sig_strikes,R_total_strikes,B_total_strikes,winner) FROM '/Users/vlad/Downloads/databases/lab-1/data/fights.csv' DELIMITER ',' CSV HEADER;
