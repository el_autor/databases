// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription
import Foundation

let package = Package(
    name: "lab-1",
    dependencies: [
        .package(
            url: "https://github.com/swiftcsv/SwiftCSV.git",
            from: "0.5.6"
        )
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "lab-1",
            dependencies: ["SwiftCSV"]),
        .testTarget(
            name: "lab-1Tests",
            dependencies: ["lab-1"]),
    ]
)
