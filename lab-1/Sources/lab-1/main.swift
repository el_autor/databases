import SwiftCSV
import Foundation

func parseHeight(_ str: String) -> Float? {
    if str.count == 0 {
        return nil
    }

    var copy = str 
    var resultNumber = ""
    resultNumber += String(copy.removeFirst())
    copy.removeLast()
    copy.removeFirst(2)
    resultNumber += "." + copy

    //print(resultNumber)
    return Float(resultNumber)
}

func parseWeight(_ str: String) -> Int? {
    if str.count == 0 {
        return nil
    }

    var copy = str
    copy.removeLast(5)
    return Int(copy)
}

func parseReach(_ str: String) -> Int? {
    var copy = str
    if copy.count == 0 {
        return nil
    }

    copy.removeLast(1)
    return Int(copy)   
}

func parseDate(_ str: String) -> String {
    if str.count == 0 {
        return ""
    }

    var copy = str

    var month = ""
    for _ in 1...3 {
        month += String(copy.removeFirst())
    }
    var monthNumber = 0

    switch month {
        case "Jan":
            monthNumber = 1
        case "Feb":
            monthNumber = 2
        case "Mar":
            monthNumber = 3
        case "Apr":
            monthNumber = 4
        case "May":
            monthNumber = 5
        case "Jun":
            monthNumber = 6
        case "Jul":
            monthNumber = 7
        case "Aug":
            monthNumber = 8
        case "Sep":
            monthNumber = 9
        case "Oct":
            monthNumber = 10
        case "Nov":
            monthNumber = 11
        case "Dec":
            monthNumber = 12
        default:
            ()
    }

    var year = ""
    for _ in 1...4 {
        year = String(copy.removeLast()) + year 
    }

    copy.removeFirst()
    copy.removeLast()
    copy.removeLast()
    let day = copy

    return "\(year)-\(String(monthNumber))-\(day)"
}

func generateName() -> String {
    return "UFC \(Int.random(in: 1...300))"
}

do {
    let csvWeightclasses = """
    name, down_bound, up_bound
    Bantamweight, 125, 135
    Women's Flyweight, 115, 125
    Lightweight, 145, 155
    Heavyweight, 225, 1000
    Women's Strawweight, 0, 115
    Featherweight, 135, 145
    Middleweight, 170, 185
    Light Heavyweight, 185, 205
    Welterweight, 155, 170
    Women's Bantamweight, 125, 135
    Women's Featherweight, 135, 145
    Flyweight, 115, 125
    Catch Weight, 0, 1000
    Open Weight, 0, 1000
    """

    var csvReferees = """
    name, experience\n
    """

    var csvFighters = """
    name, height, weight, reach, date_of_birth, wins, KOs, subs, desision, loses, draws, stance\n
    """

    var csvEvents = """
    fightday, location, name\n
    """

    var csvFights = """
    id_event, id_weightclass, id_referee, id_fighter_R, id_fighter_B, R_sig_strikes, B_sig_strikes, R_total_strikes, B_total_strikes, winner\n 
    """

    let winner = ["Red", "Blue"]

    let csv: CSV = try CSV(url: URL(fileURLWithPath: "./data/data.csv"))
    let csvRawFighters = try CSV(url: URL(fileURLWithPath: "./data/raw_fighter_details.csv"))
    var uniqueWeightclasses: Dictionary<String, Int> = [:]
    var uniqueReferees: Dictionary<String, (Int, Int)> = [:]
    var uniqueFighters: Dictionary<String, (Float?, Dictionary<String, Int?>, String, String)> = [:]
    var uniqueEvents: Dictionary<String, Int> = [:]
    var id = 1

    try csvRawFighters.enumerateAsDict { dict in
        if !uniqueFighters.keys.contains(dict["fighter_name"]!) {
            uniqueFighters[dict["fighter_name"]!] = (parseHeight(dict["Height"]!),
                                                    ["id" : id,  
                                                     "weight" : parseWeight(dict["Weight"]!),
                                                     "reach" : parseReach(dict["Reach"]!),
                                                     "wins" : 0,
                                                     "KOs" : 0,
                                                     "subs" : 0,
                                                     "desision" : 0,
                                                     "loses" : 0,
                                                     "draws" : 0 ], 
                                                     parseDate(dict["DOB"]!), dict["Stance"]!)
            id += 1
        }
    }

    var idEvents = 1
    var idWeightclass = 1
    var idReferee = 1

    try csv.enumerateAsDict { dict in
 
        if !uniqueEvents.keys.contains(dict["date"]!) {
            uniqueEvents[dict["date"]!] = idEvents
            idEvents += 1
            var location = dict["location"]!
            location.removeAll(where: {$0 == ","})
            csvEvents += "\(dict["date"]!),\(location),\(generateName())\n"
        }

        if uniqueFighters.keys.contains(dict["R_fighter"]!) {
            
            // Red corner

            if Int(Float(dict["R_wins"]!)!) > uniqueFighters[dict["R_fighter"]!]!.1["wins"]!! {
                uniqueFighters[dict["R_fighter"]!]!.1["wins"]! = Int(Float(dict["R_wins"]!)!)
            }

            if Int(Float(dict["R_win_by_KO/TKO"]!)!) > uniqueFighters[dict["R_fighter"]!]!.1["KOs"]!! {
                uniqueFighters[dict["R_fighter"]!]!.1["KOs"]! = Int(Float(dict["R_win_by_KO/TKO"]!)!)
            }

            if Int(Float(dict["R_win_by_Submission"]!)!) > uniqueFighters[dict["R_fighter"]!]!.1["subs"]!! {
                uniqueFighters[dict["R_fighter"]!]!.1["subs"]! = Int(Float(dict["R_win_by_Submission"]!)!)
            }

            let decisionWins = Int(Float(dict["R_win_by_Decision_Majority"]!)!) + Int(Float(dict["R_win_by_Decision_Split"]!)!) + Int(Float(dict["R_win_by_Decision_Unanimous"]!)!)
            if decisionWins > uniqueFighters[dict["R_fighter"]!]!.1["desision"]!! {
                uniqueFighters[dict["R_fighter"]!]!.1["desision"]! = decisionWins
            }

            if Int(Float(dict["R_draw"]!)!) > uniqueFighters[dict["R_fighter"]!]!.1["draws"]!! {
                uniqueFighters[dict["R_fighter"]!]!.1["draws"]! = Int(Float(dict["R_draw"]!)!)
            }

            if Int(Float(dict["R_losses"]!)!) > uniqueFighters[dict["R_fighter"]!]!.1["loses"]!! {
                uniqueFighters[dict["R_fighter"]!]!.1["loses"]! = Int(Float(dict["R_losses"]!)!)
            }
        }

        if uniqueFighters.keys.contains(dict["B_fighter"]!) {

            // Blue corner

            if Int(Float(dict["B_wins"]!)!) > uniqueFighters[dict["B_fighter"]!]!.1["wins"]!! {
                uniqueFighters[dict["B_fighter"]!]!.1["wins"]! = Int(Float(dict["B_wins"]!)!)
            }

            if Int(Float(dict["B_win_by_KO/TKO"]!)!) > uniqueFighters[dict["B_fighter"]!]!.1["KOs"]!! {
                uniqueFighters[dict["B_fighter"]!]!.1["KOs"]! = Int(Float(dict["B_win_by_KO/TKO"]!)!)
            }

            if Int(Float(dict["B_win_by_Submission"]!)!) > uniqueFighters[dict["B_fighter"]!]!.1["subs"]!! {
                uniqueFighters[dict["B_fighter"]!]!.1["subs"]! = Int(Float(dict["B_win_by_Submission"]!)!)
            }

            let decisionWins = Int(Float(dict["B_win_by_Decision_Majority"]!)!) + Int(Float(dict["B_win_by_Decision_Split"]!)!) + Int(Float(dict["B_win_by_Decision_Unanimous"]!)!)
            if decisionWins > uniqueFighters[dict["B_fighter"]!]!.1["desision"]!! {
                uniqueFighters[dict["B_fighter"]!]!.1["desision"]! = decisionWins
            }

            if Int(Float(dict["B_draw"]!)!) > uniqueFighters[dict["B_fighter"]!]!.1["draws"]!! {
                uniqueFighters[dict["B_fighter"]!]!.1["draws"]! = Int(Float(dict["B_draw"]!)!)
            }

            if Int(Float(dict["B_losses"]!)!) > uniqueFighters[dict["B_fighter"]!]!.1["loses"]!! {
                uniqueFighters[dict["B_fighter"]!]!.1["loses"]! = Int(Float(dict["B_losses"]!)!)
            }
        }
        
        if !uniqueWeightclasses.keys.contains(dict["weight_class"]!) {
            uniqueWeightclasses[dict["weight_class"]!] = idWeightclass
            idWeightclass += 1
        }

        if dict["Referee"]! != "" {

            if !uniqueReferees.keys.contains(dict["Referee"]!) {
                uniqueReferees[dict["Referee"]!] = (idReferee, 1)
                idReferee += 1
            }
            else {
                uniqueReferees[dict["Referee"]!]!.1 += 1
            }
        }
    }

    for (name, exp) in uniqueReferees {
        csvReferees += "\(name), \(exp.1)\n"
    }

    for (key, value) in uniqueFighters {
        csvFighters += "\(key),"
        if value.0 != nil {
            csvFighters += String(value.0!)
        } 
        csvFighters += ","
        if value.1["weight"]! != nil {
            csvFighters += String(value.1["weight"]!!)
        }
        csvFighters += ","
        if value.1["reach"]! != nil {
            csvFighters += String(value.1["reach"]!!)
        }
        csvFighters += ",\(value.2),"
        csvFighters += "\(value.1["wins"]!!),\(value.1["KOs"]!!),\(value.1["subs"]!!),\(value.1["desision"]!!),\(value.1["loses"]!!),\(value.1["draws"]!!),"
        csvFighters += "\(value.3)\n"
    }
    
    try csv.enumerateAsDict { dict in 
        var idEvent = ""
        var idWeightclass = ""
        var idReferee = ""
        var idFighterR = ""
        var idFighterB = "" 

        if dict["Referee"]! != "" {
            idEvent = String(uniqueEvents[dict["date"]!]!)
            idWeightclass = String(uniqueWeightclasses[dict["weight_class"]!]!)
            idReferee = String(uniqueReferees[dict["Referee"]!]!.0)

            if uniqueFighters[dict["R_fighter"]!] != nil && uniqueFighters[dict["B_fighter"]!] != nil {
                idFighterR = String(uniqueFighters[dict["R_fighter"]!]!.1["id"]!!)
                idFighterB = String(uniqueFighters[dict["B_fighter"]!]!.1["id"]!!)
                let RSigStrikes = String(Int.random(in: 100...200))
                let BSigStrikes = String(Int.random(in: 100...200))
                let RTotalStrikes = String(Int.random(in: 100...200))
                let BTotalStrikes = String(Int.random(in: 100...200))

                csvFights += idEvent + "," + idWeightclass + "," + idReferee + "," + idFighterR + "," + idFighterB + "," + RSigStrikes + "," + BSigStrikes + "," + RTotalStrikes + "," + BTotalStrikes + "," + winner[Int.random(in: 0...1)] + "\n"
                
            }
        }
    }

    //print(csvEvents)

    let fm = FileManager.default
    fm.createFile(atPath: "./data/weightclasses.csv", contents: csvWeightclasses.data(using: .utf8))
    fm.createFile(atPath: "./data/referees.csv", contents: csvReferees.data(using: .utf8))
    fm.createFile(atPath: "./data/fighters.csv", contents: csvFighters.data(using: .utf8))
    fm.createFile(atPath: "./data/events.csv", contents: csvEvents.data(using: .utf8))
    fm.createFile(atPath: "./data/fights.csv", contents: csvFights.data(using: .utf8))

    /*
    let csv: CSV = try CSV(url: URL(fileURLWithPath: "./data/raw_fighter_details.csv"))
    try csv.enumerateAsDict { dict in
        print(dict["Reach"])
    }
    */
} 
catch {
    print(error)
}

